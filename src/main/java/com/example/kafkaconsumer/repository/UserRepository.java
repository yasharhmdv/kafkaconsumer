package com.example.kafkaconsumer.repository;

import com.example.kafkaconsumer.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
