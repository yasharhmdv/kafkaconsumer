package com.example.kafkaconsumer.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaSmsListener {

    @KafkaListener(topics = {"sms-event-topic"})
    public void listen(String message) {
        log.info("Message received: {}", message);
    }
}
