package com.example.kafkaconsumer.listener;

import com.example.kafkaconsumer.model.User;
import com.example.kafkaconsumer.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserUpdateBalanceListener {

    private final UserRepository userRepository;

    @KafkaListener(topics = "user-update-balance", containerFactory = "kafkaJsonListenerContainerFactory")
    public void listen(User user) {
        log.info("UserDto received like {}", user);

        User userFromDb = userRepository.findById(user.getId()).orElseThrow(
                () -> new RuntimeException("User not found"));

        userFromDb.setBalance(user.getBalance());
        log.info("balance is {}", userFromDb.getBalance());
        userRepository.save(userFromDb);
    }
}
