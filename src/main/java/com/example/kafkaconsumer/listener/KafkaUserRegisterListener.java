package com.example.kafkaconsumer.listener;

import com.example.kafkaconsumer.dto.UserDto;
import com.example.kafkaconsumer.model.User;
import com.example.kafkaconsumer.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaUserRegisterListener {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @KafkaListener(topics = {"user-register"}, containerFactory = "kafkaJsonListenerContainerFactory")
    public void listen(UserDto userDto) {
        log.info("UserDto received like {}", userDto);
        User user = modelMapper.map(userDto, User.class);
        log.info("USER MODIFIED:{}", user);
        userRepository.save(user);
    }
}

